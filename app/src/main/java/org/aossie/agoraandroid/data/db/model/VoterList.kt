package org.aossie.agoraandroid.data.db.model

data class VoterList(
  var name: String? = null,
  var hash: String? = null
)